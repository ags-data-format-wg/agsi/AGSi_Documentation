# AGSi Guidance / General

## Introduction to the guidance

The guidance provides advice on how to use AGSi, including recommendations for best practice.

**The [Introduction to AGSi](./Guidance_Brief_Scope.md) guidance is the ideal place to start for those new to AGSi.
This short guide (total read time 20-25 minutes) explains what AGSi is and how it works.**

The guidance is not part of the standard. In the event of conflict, the standard takes precedence.

Each page of the guidance is version controlled separately, as indicated on the relevant page, as it is on this page (see
[version history](#version-history) below).

Unless otherwise stated on the page, the version of the standard that the guidance is applicable to is the current version.


### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.