# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 19:02:13 2024

@author: DigitalGeotechnical
On behalf of AGSi subgroup
"""

# Licence: LGPL3

# This script creates the AGSi template files provided in 'Tools' on the AGSi Guidance website
# Written in such a way that it should also eventually work for AGS Piling (but may need some tweaks - not tested yet)

# This file was written for internal AGS use only. It is not designd to be used by others.
# If you use it, you do so at your own risk.
# WARNING - this script includes some simplications (some documented, but don't rely on this being complete!)
# that are 'ok' at present, but may be invalid if we make significant changes to AGSi (or AGS Piling) going forward.
# Therefore, use with care. If in doubt, talk to us (via Gitlab - or email ags@ags.org.uk).
# If you do find this useful (and fork it) then please let us know - out of courtesy. 

import os
import json
from tkinter import Tk, filedialog

# Main function that does the heavy lifting
# This works recursively!
# This does the template file, both blank and example versions
# Assumptions
# 'allOf' keyword not covered (will be ignored, data lost)
# 'anyOf' not covered for root (ok for now) but is covered beyond this
# Null object type not covered (do not currently have any) - Not currently used in AGSi so ok for AGSi right now.
# Required fields - we do nothing with these here (we can't - this is for templates - not validation!)
 
def getobjdict(v, d, OutputExamples):
    objname = v['$ref'].split('/')[2]
    objdict = {}
    print(objname) # Just here to show progress

    for attr, val in d[objname]['properties'].items():
        if val['type'] in ['string','number','boolean']:
            # This is the line that writes the value!
            if OutputExamples:
                objdict[attr] = val.get('example', 'No example given')
            else:
                if val['type'] == 'boolean':
                    objdict[attr] = False
                elif val['type'] == 'number':
                    objdict[attr] = 0
                else: #String,
                    objdict[attr] = ''
        elif val['type'] == 'object':
            if '$ref' in val:
                objdict[attr] = getobjdict(val, d, OutputExamples)
            elif 'anyOf' in val:
                # Take the first of the possible objects only!!!!!
                # For mogdel element agsiGeometry this is set to be a vol from surfaces object
                # (which in turn embeds geometry from file objects)
                objdict[attr] = getobjdict(val['anyOf'][0], d, OutputExamples)
        elif val['type'] == 'array':
            objdict[attr] = []
            if 'items' in val: # Usually just one item
                # Need to intercept if items are 'coordinateTuple'  - short term bodge!
                if val['items'] == {'$ref': '#/$defs/coordinateTuple'}:
                    if OutputExamples:
                        objdict[attr] = val.get('example', 'No example given')
                    else:
                        objdict[attr] = [[0,0,0],[0,0,0]]
                else:
                    arrayobj = getobjdict(val['items'], d, OutputExamples)
                    objdict[attr].append(arrayobj)
            #elif 'items_special' in val: # our hacked case for agsiModelElementbled for now
            #    for item in val['items_special']:
            #        arrayobj = getobjdict(item, d)
            #        objdict[attr].append(arrayobj)
            elif 'anyOf' in val:
                objdict[attr] = []
                for item in val['anyOf']:
                    arrayobj = getobjdict(item, d, OutputExamples)
                    objdict[attr].append(arrayobj)
            elif 'example' in val:
                if OutputExamples:
                    objdict[attr] = val['example']
                else:
                    if attr == 'valueProfile':
                        objdict[attr] = [[0,0],[0,0]]
                    elif attr == 'profileCoordinates': # Not sure this gets used any more - intercepted above
                        objdict[attr] = [[0,0,0],[0,0,0]]
                    elif attr in ['topCoordinate','coordinate']:
                        objdict[attr] = [0,0,0]
                    else:
                        objdict[attr] = [0,0]

    return objdict

# This function creates the simple list of objects, i.e template for individual object
# (which is actually a dict so user knows which is which - but we used dict above so lets call this list)
def getobjlist(d, OutputExamples):
    objlist = {}
    for k, v in d.items():
        print(k)
        if 'properties' in v: # Check its a normal object
            objlist[k] = {}
            for a in v['properties']:
                adict = v['properties'][a]
                # For standard attribute with example
                if 'example' in adict:
                    if OutputExamples:
                        objlist[k][a] = adict['example']
                    else:
                        if adict['type'] == 'string':
                            objlist[k][a] = ''
                        elif adict['type'] == 'number':
                            objlist[k][a] = 0
                        elif adict['type'] == 'boolean': 
                            objlist[k][a] = adict['example'] # Use example!  
                        elif adict['type'] == 'object':
                            objlist[k][a] = {}
                        elif adict['type'] == 'array':
                            if a == 'valueProfile':
                                objlist[k][a] = [[0,0],[0,0]]
                            elif a == 'profileCoordinates': # Not sure this gets used any more - intercepted above
                                objlist[k][a] = [[0,0,0],[0,0,0]]
                            elif a in ['topCoordinate','coordinate']:
                                objlist[k][a] = [0,0,0]
                            else:
                                objlist[k][a] = [] 
                else:
                   if adict['type'] == 'object':
                       if OutputExamples:
                           objlist[k][a] = { 'Replace this object with' : adict['description'] }
                       else:
                           objlist[k][a] = {}
                   elif adict['type'] == 'array':
                       if OutputExamples:
                           objlist[k][a] = [adict['description'] ]
                       else:
                           objlist[k][a] = []
        else:
            print('Not included:' + k)
      
    return objlist    

# Special version of the above for root object
def getobjlistroot(s, OutputExamples):
    objlist = {}
    objlist['root'] = {}
    for k, v in s['properties'].items():
        if OutputExamples:
            if '$ref' in v:
                targetobj = v['$ref']
                targetobj = targetobj.replace('#/$defs/','')
                objlist['root'][k] = {'Replace this object with': targetobj + ' object'}
            elif v.get('type', None) == 'array':
                targetobj = v['items']['$ref']
                targetobj = targetobj.replace('#/$defs/','')
                objlist['root'][k] = ['Array of embedded '+ targetobj + ' object(s)']
        else:
            if '$ref' in v:
                objlist['root'][k] = {}
            elif v.get('type', None) == 'array':
                    objlist['root'][k] = []
    return objlist

################# Main execution from here...

# Get JSON Schema file and target folder for output
root = Tk()
schemafile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select input JSON Schema file',
                                    filetypes = (('Excel files','*.json'),
                                                 ('Excel files','*.JSON')))
#Correct file names hard way until fix found
schemafilepathname = schemafile.replace('/','\\')
#inputfolder = os.path.dirname(inputfile)

# Select folder to save as (will prompt if already exists)
outputfolder=filedialog.askdirectory(initialdir = '../',
                                   title = 'Folder for template files created')

# End of input
root.destroy()  # Gets rid of annoying Tk window

# Load JSON Schema to dict    
with open(schemafilepathname,'r') as jsondata:
    s = json.load(jsondata)

# In practice we will be using just the object definitions, which are found here:
d = s['$defs']

# Try to determine schema and version from file JSON Schema provided
# Use 'title' of schema as root of filenames to be created, except sub _ for space and - for .
basefilename = s.get('title','schemaname_X-X-X')
basefilename = basefilename.replace(' ','_')
basefilename = basefilename.replace('.','-')


for OutputExamples in [True, False]:
    # False to output blank data     
        
    root = {}
    
    for k, v in s['properties'].items():
        if '$ref' in v:  
            root[k] = getobjdict(v, d, OutputExamples)
        elif v['type'] == 'array':
            root[k] = []
            if 'items' in v: # Applies if only one object class allowed - only case in AGSi, for now
                arrayobj = getobjdict(v['items'], d, OutputExamples)
                root[k].append(arrayobj)
    
    #Write Json from dict to file
    
    if OutputExamples:
        filename = basefilename + '_template_example_data.json'
        outputfilepathname = os.path.join(outputfolder, filename)
    else:            
        filename = basefilename + '_template_blank_data.json'
        outputfilepathname = os.path.join(outputfolder, filename)
        
    with open(outputfilepathname, 'w') as jsonfile:
        json.dump(root, jsonfile, indent=4)
        
    # Get simple object list
    # Start with root object
    objlistroot = getobjlistroot(s, OutputExamples)
   
    # Now main run 
    objlistmain = getobjlist(d, OutputExamples)
    
    # Combine them
    objlist = objlistroot | objlistmain
    
    if OutputExamples:
        filename = basefilename + '_objects_example_data.json'
        outputfilepathname = os.path.join(outputfolder, filename)
    else:            
        filename = basefilename + '_objects_blank_data.json'
        outputfilepathname = os.path.join(outputfolder, filename)
        
    with open(outputfilepathname, 'w') as jsonfile:
        json.dump(objlist, jsonfile, indent=4)
  
print('All done')