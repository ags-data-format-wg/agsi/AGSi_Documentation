# Guidance / Tools
## AGSi template files

### Overview

| What is it? | Set of AGSi template files that may be useful for assembling AGSi JSON in a text editor. Template JSON for all AGSi objects is provided.|
| --------- | |
| Who is it by? | AGSi team |
| Is it free? | Yes |
| Licence | Open source, <a href="https://www.gnu.org/licenses/lgpl-3.0.en.html" target="_blank">LGPL3</a> |
| Source code available? | Yes. See below. |
| Full information | Below |

### Introduction

Sometimes it may be useful to create or modify AGSi files in a text editor, in particular when developing and testing tools/scripts for working with AGSi.

To assist this process, the AGSi team thought it would be useful to provide some 'template' AGSi JSON to use as a starting point, i.e. syntactically correct AGSi that could be cut/paste and then modified to form working AGSi files. For this, it is assumed that the user will be familiar with JSON syntax.

### Description of the template files

A few different versions of the 'template' are provided, to suit different ways of working. These are:

- Fully formed template AGSi file with all attributes for all objects, using the 'example' values given in the documentation
- Fully formed template AGSi file with all attributes for all objects, but with blank values (or 0 for numeric values)
- Flat dictionary of all AGSi objects, again using the 'example' values given in the documentation
- Flat dictionary of all AGSi objects, but with blank or 0 values

When working with these files, please be aware of the following:

- The template files include embedded objects as applicable. The flat dictionaries of objects do not.
- Only one instance of each object (including embedded objects) is provided in all cases.
- Where a single object is embedded but this object may be from one of several object classes, the example given in the template corresponds to the first such object listed in the documentation (see Note below for practical significance of his).
- The 'blank' version out of necessity includes `0` for numeric values because blank `""` string values would be invalid (wrong data type). However, this means that the user must ensure that unwanted or unused zero values are modified, or the attribute entirely deleted, as required. Null entries are not permitted in AGSi (at present).
- The template files may not, by themselves, pass validation without errors. This is because it is not possible to ensure that all cross-references are valid within the template files themselves.

!!! Note
    The limitation applicable to single objects significantly impacts on the
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a> attribute of 
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a>.
    The example in the template assumes an
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a>
    object, which in turn includes embedded 
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a> objects under the 
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrytop">*agsiGeometryTop*</a> and
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces#agsigeometrybottom">*agsiGeometryBottom*</a>
    attributes. However, other objects or combinations of objects are also acceptable for 
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a>.
    Check the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">documentation</a> for details.


### Link to template files

The files (for AGSi v1.0.1) [are provided here](./tools/Templates/AGSi_v1-0-1_templatefiles.zip). These can also be safely used for v1.0.0 (some example values have changed but this should not matter).

### Source code

The files are the files, therefore there is no source code as such. However, we are happy for you to have a look at the code (in Python) that is used to create the template files. Note that the input used by this script is the relevant AGSi JSON Schema file.

To see this code, go to our <a href="https://gitlab.com/ags-data-format-wg/agsi/AGSi_Documentation" target="_blank">Gitlab repo (Guidance)</a>
then navigate to `docs > tools > Template` and you should find the python script(s) used.


### Version history

Page added June 2024 (for v1.0.1 release).
