# AGSi Guidance / Models

## Models showing exploratory holes - Worked Example

Here is an example showing how to model exploratory holes, including their geology.
For guidance on the underlying principles and use of the schema, refer to
[the previous page](./Guidance_Model_Boreholes.md)

In this simplified example, there are two boreholes with geology (three
geological unit segments in each), as illustrated below.
For one of the boreholes a profile of SPT N values with depth is provided.

![Boreholes with geology example](./images/Guidance_Model_Boreholes_Example.png)

A copy of the full JSON data file described below can be downloaded
[here](./exampledata/AGSi_Guidance_Examples.zip).

!!! Note
    In the following the JSON data is broken up in to several sections to allow commentary to be provided. To ensure that the JSON data is formatted correctly it has been necessary to include an additional "{" or "[" at the top and/or bottom of some of the sections. For the correct version of full JSON file please use the link posted above.


#### Model metadata

This first section contains model metadata in the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object. For the purposes of this example, the holes are provided in their own standalone model. However, it should be noted that exploratory holes may be incorporated into other relevant models, e.g. the main geological (volume) model.

``` json
    {
        "agsiModel": [
            {
                "modelID": "ffcaa660-69b8-412a-9d32-dc906195bc0e",
                "modelName": "Sitewide geological model - boreholes only",
                "description": "C999 Package X Sitewide geological model - boreholes only.",
    			"coordSystemID": "MetroXYZ",
                "modelType": "Geological model",
                "category": "Observational",
                "domain": "Engineering geology",
}]}
```

#### Observation set

We will be using <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">observation group</a>
objects for the exploratory holes and their data, so we first need to establish an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>
object within the parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a>.

Here we use the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet#investigationid">*investigationID*</a>
attribute to reference the identifier of the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a>
object for the relevant ground investigation (we define this later).

``` json
{
                "agsiObservationSet": [
                    {
                        "observationSetID": "Obs/GIHolesA",
                        "description" : "2018 GI Package A",
                        "investigationID": "GI Package A",
}]}
```

#### Exploratory holes

We then define the exploratory holes using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a>
objects embedded (as an array) within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>.
The first of our holes is defined in the snippet below.

``` json
{
                        "agsiObservationExpHole": [
                            {
                                "holeID": "A/BH01",
                                "holeName": "BH01",
                                "topCoordinate": [525275.5,181543.2,15.3],
                                "verticalHoleDepth": 25,
                                "holeType": "CP",
                                "date": "2018-05-23",
}]}
```

#### Geology (column segments)

The geological succession for this hole is now provided using an array of
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn">agsiObservationColumn</a>
objects embedded within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a>.
In this example there is one segment object for each of the units encountered in the hole.

The codes used by
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn#legendcode">*legendCode*</a>
and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn#geologycode">*geologyCode*</a>
are defined later.

``` json
{
                                "agsiObservationColumn": [
                                    {
                                        "topElevation": 15.3,
                                        "bottomElevation": 13.5,
                                        "description": "Very soft and soft brown, dark brown and grey slightly sandy slightly gravelly CLAY",
                                        "legendCode": "102",
                                        "geologyCode": "MG"
                                    },
                                    {
                                        "topElevation": 13.5,
                                        "bottomElevation": 8.9,
                                        "description": "Soft brown silty sandy  CLAY",
                                        "legendCode": "207",
                                        "geologyCode": "ALV"
                                    },
                                    {
                                        "topElevation": 8.9,
                                        "bottomElevation": -9.7,
                                        "description": "Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (<0.1m thick)",
                                        "legendCode": "202",
                                        "geologyCode": "GC"
                                    }
                                ],
}
```

### Test (SPT) data

We now include some SPT N values for this hole, using the AGSi recommended approach of embedding an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a>
object within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a>.
The 'profile ' of N values within the hole is provided using the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue#valueprofile">*valueProfile*</a>
attribute in which we provide an array of ``[Elevation,SPT N]`` pairs.
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue#valueprofileindvarcodeid">*valueProfileIndVarCodeID*</a>
is used to confirm that the profile is defined by *elevation*.

!!! Note
    This example should not be taken as encouragement to include SPT data in a model. If there are good reasons for doing so, and it is agreed that it should be included, then ok. But if only SPTs are included, and not other test data that is just as important, then it should be questioned.


``` json
{
                                "agsiDataPropertyValue": [
                                    {
                                        "codeID": "ISPT_NVAL",
                                        "valueProfileIndVarCodeID": "Elevation",
                                        "valueProfile": [
                                            [14.0,21],
                                            [13.0,8],
                                            [12.0,7],
                                            [11.0,27],
                                            [10.0,38],
                                            [8.5,22]
                                        ],
                                        "remarks": "Only a few reading shown here, for brevity"
                                    }
                                ]

},
```

### Another hole

Here is the data for another hole, i.e. a new object within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>.
There is no SPT data included this time.


``` json

                            {
                                "holeID": "A/BH02",
                                "holeName": "BH02",
                                "topCoordinate": [525220.0,181550.0,17.2],
                                "verticalHoleDepth": 40,
                                "holeType": "CP+RC",
                                "date": "2018-05-27",
                                "agsiObservationColumn": [
                                    {
                                        "topElevation": 17.2,
                                        "bottomElevation": 14.1,
                                        "description": "Brown slightly clayey SAND with some subangular to subrounded fine to coarse fravel of flint, concrete and brick. Occasional subangular cobbles of concrete. ",
                                        "legendCode": "102",
                                        "geologyCode": "MG"
                                    },
                                    {
                                        "topElevation": 14.1,
                                        "bottomElevation": 9.4,
                                        "description": "Very soft/loose brown sandy SILT",
                                        "legendCode": "303",
                                        "geologyCode": "ALV"
                                    },
                                    {
                                        "topElevation": 9.4,
                                        "bottomElevation": -22.8,
                                        "description": "Stiff to very stiff blue grey silty CLAY",
                                        "legendCode": "202",
                                        "geologyCode": "GC"
                                    }
                                ]
                            }
]}]}],

```

### Investigation

In the [Exploratory holes](#exploratory-holes) snippet above we referenced the identifier of an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a>
object. We now define that object.

However, before doing so we must first define a parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProject">agsProject</a> object, in which <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a> is embedded.

``` json
    {
        "agsProject": {
            "projectName": "C999 Geotechnical Package X",
            "remarks": "Other project metadata omitted for brevity",
            "agsProjectInvestigation": [
                {
                    "investigationID" : "GIPackageA",
                    "investigationName" : "Gotham City Metro Purple Line, C999 Package A",
                    "contractor" : "Boring Drilling Ltd",
                    "remarks" : "Other GI metadata omitted for brevity"
                }
            ],
}}
```

### Codes

Finally, we provide the definitions for the codes that have been used in some of the data. Specifically:

- data property codes
- geology codes
- legend codes
- exploratory hole type codes

!!! Note
    The codes are defined at the end in this example, but this does not have to be the case as the order of objects in AGSi is not important. It would be acceptable to include this block of data at the start of the file.

The codes are defined using <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a>.

We will start with the codes used by <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a>.
First we establish the 'set' of codes using an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a> object.
In this example the codes used are in accordance with the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Codes_Codelist">AGSi standard code list</a>,
which is turn is based on the
<a href="https://www.ags.org.uk/data-format/ags4-data-format/ags-4-0-data-format/abbreviations/" target="_blank">AGS (factual) format ABBR standard code list</a>.

The codes themselves are defined using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a> objects embedded as an array within
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a>.


``` json
{
            "agsProjectCodeSet": [
                {
                    "description": "Property codes (AGS)",
                    "usedByObject": "agsiDataPropertyValue",
                    "usedByAttribute": "codeID",
                    "sourceDescription": "AGS ABBR standard code list",
                    "sourceURI": "https://www.ags.org.uk/data-format/ags4-data-format/ags-4-0-data-format/abbreviations/",
                    "agsProjectCode": [
                        {
                            "codeID": "ISPT_NVAL",
                            "description": "SPT N value",
                            "units": "Blows/300mm",
                            "remarks": "N value extrapolated where test terminated before full penetration"
                        },
                        {
                            "codeID": "Elevation",
                            "description": "Elevation",
                            "units": "mOD"
                        }
                    ]
                },
]}
```
Now we define a set of geology codes. In this example, these are project specific codes.

``` json
                {
                    "description": "Geology codes",
                    "usedByObject": "agsiObservationColumn",
                    "usedByAttribute": "geologyCode",
                    "sourceDescription": "Project geology code list",
                    "agsProjectCode": [
                        {
                            "codeID": "MG",
                            "description": "Made Ground"
                        },
                        {
                            "codeID": "ALV",
                            "description": "Alluvium"
                        },
                        {
                            "codeID": "GC",
                            "description": "Gotham Clay"
                        }
                    ]					
                },
```

Finally the legend and hole type codes. Here we will simply reference the
<a href="https://www.ags.org.uk/data-format/ags4-data-format/ags-4-0-data-format/abbreviations/" target="_blank">AGS (factual) format ABBR standard</a>
list of codes for each of these, given that they are well known.

!!! Note
        It would be arguably better practice to also include the codes themselves here. For the sake of brevity, we have not done so for this example.


``` json
                {
                    "description": "Legend codes",
                    "usedByObject": "agsiObservationColumn",
                    "usedByAttribute": "legendCode",
                    "sourceDescription": "AGS ABBR standard code list",
                    "sourceURI": "https://www.ags.org.uk/data-format/ags4-data-format/ags-4-0-data-format/abbreviations/"
                },
                {
                    "description": "Exploratory hole types",
                    "usedByObject": "agsiObservationExpHole",
                    "usedByAttribute": "holeType",
                    "sourceDescription": "AGS ABBR standard code list",
                    "sourceURI": "https://www.ags.org.uk/data-format/ags4-data-format/ags-4-0-data-format/abbreviations/"
                }
]
            ]
        }
    }
```

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.
