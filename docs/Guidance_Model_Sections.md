# AGSi Guidance / Models

## Models using sections/fence/profile diagrams

Models may comprise or include sections and/or fence diagrams and/or 2D profiles in the following ways:

1.  A 3D volume model may include sections/fence diagrams within it as a way
    of illustrating how the 3D model has been derived
    or as additional complementary 2D views of the model at specific locations.
2.  The model may be formed entirely by a section,
    e.g. a geological profile along a linear infrastructure alignment
3.  Profiles, effectively standalone sections, used as input for
    analysis software.

The above are illustrated in the following sketches.

**(1) 3D section/fence diagrams**

![3D section](./images/Guidance_Model_Section_3DSection.png)

**(2) 2D section, with exploratory holes**

![2D section](./images/Guidance_Model_Section_2DSection.png)

**(3) Profile, e.g. input to slope stability analysis**

![Profile section](./images/Guidance_Model_Section_Profile.png)

All of the above are supported by AGSi.
Further explanation provided below.

### Terminology: section or fence or profile, 2D or 3D?

Research carried out for the purposes of AGSi has shown that usage of and,
where they appear, definitions of the terms **section** and **fence** are
inconsistent. They are often used interchangeably.

This inconsistency is not critical to the implementation of AGSi. However,  
for the purposes of AGSi documentation, the following convention has been adopted:

* A **fence** diagram is drawn along a line that connects exploratory holes.
  Lines interpreting the geology between holes may be drawn between the holes.
  The lines should coincide with the boundaries seen in the holes.
* A **section** (of which cross sections and longitudinal sections are
  particular types) can be drawn along any line.
  Profiles from exploratory holes may be projected on to the section line but the lines interpreting the geology along the
  section line will not normally coincide with the boundaries seen in
  holes given that the holes may be offset from the section line.

Sometimes, in particular where 2D geometry is required for input to
analysis software, the term **profile** is used. In practice a profile is the same as a section, but its usage and what it represents may differ,
eg. a profile is likely to represent design geometry
(<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#analytical-model">analytical</a> category) whereas a section is more likely to be
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#observational-model">observational</a>.

Using the above definitions, it can be seen that a fence diagram is a particular type of section, and a profile is also a section.

!!! Note
    It is acknowledged that there are alternative definitions.
    The above is based on a consensus reached by the AGSi authors at time of writing.
    If you strongly disagree then you are welcome to contact us!

Both fence diagrams and sections may be represented in AGSi.

!!! Note
    For the remainder of this page, the term section is used
    to generally mean a section, fence diagram or profile. Any exceptions to
    this are clearly stated.  

In addition, both may be presented in either 2D space (generally a vertical plane, as chainage vs elevation) or in 3D space.

!!! Note
    For 3D sections is it recommended that the section should be
    drawn vertically, i.e. avoid inclined areas.


### Modelling sections - principles

A section in AGSi may be made up from:

* Areas, formed by lines, representing the geological interpretation along the section line
* Exploratory holes located on or projected on to the section (if required)
* Line or plane showing the location and alignment of the section line (optional)

#### Coordinate systems

Both 2D and 3D sections are supported in AGSi.

3D sections should share the same coordinates as any 3D model defined.
All elements within the 3D section are defined using the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model-coordinate-system">model coordinate system</a>.

2D sections have their own 'local' co-ordinate system representing the horizontal (chainage or baseline distance) and vertical (elevation) axes.
Vertical axis values should be the same as for the 3D model if applicable.
The horizontal axis value for any given point will differ between sections. This means that each section will have its own unique coordinate system.

As a consequence of this, each section must be defined within a separate <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">AGSi model (object)</a>,
given that a model in AGSi may only have a single coordinate system.

!!! Note
    A 2D section does not, by default, carry any knowledge of its
    location or alignment within a 3D model or the world.
    This information needs to
    be provided separately, either via metadata or reference to an **alignment** defined in a different model. This is discussed further below.

#### Areas representing geological units

The areas on a section representing the geological interpretation are each considered to be a <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model-element">model element</a>.

These elements may be defined using either of the following methods (illustrated below):

1. Top and/or bottom lines bounding an area, similar to the method used to define volumes implied from surfaces
2. A closed polygon  

!!! Note
    It is possible to define the geology by using only top lines (or bottom lines)
    with the base of area then implied to be the next top line encountered working downwards
    (or the top of the area the next bottom line working upwards).
    However, it is recommended that BOTH top and bottom
    lines are defined to minimise the likelihood of ambiguity or error.

Method 1:

![Section area from top/bottom lines](./images/Guidance_Model_Section_Area1.png)  

Method 2:

![Section area from polygon](./images/Guidance_Model_Section_Area2.png)  

In principle, these areas may be defined using 2D or 3D co-ordinates.
For 3D sections it is recommended that sections should always be
vertically aligned.

#### Exploratory holes on sections

Exploratory holes may be represented as described in the
[guidance on showing exploratory holes](./Guidance_Model_Boreholes.md).
For 3D sections the exploratory holes are the same as those
shown as part of the 3D volumetric model, if applicable.

For 2D sections, the co-ordinate system for the section
will inevitably differ from that used for the 3D model (if applicable).
Therefore, any exploratory hole to be shown on the section will need to
have its co-ordinates defined using the section (model) co-ordinate system.
The elevation (Z co-ordinate) will normally be the same in both systems.

On 2D fence diagrams, the exploratory holes lie on the line of the section.
However, on 2D sections the exploratory holes may not lie on the line of the section.
In such cases is it common practice to project the exploratory hole onto the section line.
If this is the case, then it should be made clear that the exploratory hole
positions shown are projections, not actual locations.
It is good practice to state the offset distance and direction from the section line.

#### Alignment (line of section)

AGSi includes an **alignment** object that can be used to show the location and alignment of a section line, provided that the section is cut vertically, as recommended. The *alignment* object can include metadata relevant to the section line, such as its name.

For 3D sections, the location and alignment of the section is defined implicitly. However, the user may elect to add an *alignment* to identify and mark the line of the section,
should they wish to do so.

The plan location of 2D sections is not apparent from the data provided
for the section itself. Therefore further information is required.
As a minimum, this could be provided via metadata,
e.g. reference to the location of the section shown on a drawings.

If there is already a 3D model defined then an *alignment* can be used to show the location of the section line in the 3D model. To link the 2D section model to the *alignment*, it will be necessary to reference the alignment ID from the 2D section model (an attribute is provided for this purpose).

Alternatively, if there is no 3D model then a 2D 'map' model could be used for alignments.

!!! Note
    Whilst 2D plans, i.e. maps, are not intended as one of the main use cases
    for AGSi, there is nothing to stop AGSi being used for carrying simple plan data.

An *alignment* is defined using 2D (XY) coordinates only. It does not have Z coordinates. When looking 'down', it will be seen as a line. In 3D, its vertical extent is unlimited. How the *alignment* is visualised in 3D models may be determined by the user or, more likely, the user's software. Options include showing the *alignment* as a vertical surface drawn along the alignment, or a line draped along the top surface of the model.  

Where an *alignment* is used, the the start of the line, ie. first point defined,
will by default be interpreted to correspond to the zero chainage or baseline distance.
However, the *alignment* object includes an attribute that can be used to redefine the chainage at the start of the line, should this be required.

#### Line geometry in AGSi

Geometry for lines must be provided in
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-supporting-files">supporting files</a>.

Convenient simple text file formats include:

- <a href="https://geojson.org/" target="_blank">GeoJSON</a>
- Well-known text (WKT), ISO/IEC 13249-3

GeoJSON is only able to support 2D co-ordinates. WKT can support both 2D and 3D co-ordinates.

This is an example of how a line can be defined using GeoJSON (this text constitutes a valid GeoJSON file):

```JSON
{
     "type": "LineString",
     "coordinates": [
         [0.0, 15.4],
         [21.7, 15.6],
         [38.4, 14.8],
         [50.0, 14.5]
     ]
}
```

The same line defined in WKT would look like this:

```
LINESTRING (0.0 15.4, 21.7 15.6, 38.4 14.8, 50.0 14.5)
```

It is possible to have a GeoJSON file that describes only a single line. However, if many lines are required, it may be inconvenient to have lots of files. An alternative approach would be to use a GeoJSON *feature collection* to house many geometries in the same file, each differentiated with their own *id*.
The <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile#filepart">*filePart*</a>
attribute in <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
can then be used to identify which part of each file corresponds to a particular line.

If this methodology is used, the GeoJSON would look something like this:

```JSON
    {
       "type": "FeatureCollection",
       "features": [
           {
               "type": "Feature",
               "id": "MGTop",
               "geometry": {
                   "type": "LineString",
                   "coordinates": [
                       [0.0, 15.4], [21.7, 15.6], [38.4, 14.8],  [50.0, 14.5]
                   ]
               }
           },
           {
               "type": "Feature",
               "id": "ALVTop",
               "geometry": {
                   "type": "LineString",
                   "coordinates": [
                       [0.0, 11.2], [15.3, 10.3], [29.9, 10.4],  [38.5, 10.9], [50.0, 12.2]
                   ]
               }
           }
       ]
    }
```


Caution should be exercised if this latter method is used as it may not be supported by some software. If GeoJSON is expected to be used, then this should be addressed in the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Specification">specification</a>.

WKT supports geometry 'collections' but they can not be separately identified within a file using the above methodology, so separate files containing WKT for each line will be required.


The <a href="https://datatracker.ietf.org/doc/html/rfc7946" target="_blank">GeoJSON Specification (RFC 7946)</a>
clause 4 requires *"The coordinate reference system for all GeoJSON coordinates is a geographic coordinate reference system, using the World Geodetic System 1984 (WGS 84) [WGS84] datum, with longitude and latitude units of decimal degrees."*

However, later in the same clause it states *"However, where all involved parties have a prior arrangement, alternative coordinate reference systems can be used without risk of data being misinterpreted."*

On this basis, use of
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model-coordinate-system">model coordinate system</a>
coordinates in GeoJSON files referenced by AGSi may be considered to be a *prior arrangement*!

The file formats to be used for the project should be described in the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Specification">specification</a>.

!!! Note
    An earlier draft version of AGSi had an object *agsiGeometryLine* that could handle 2D and 3D line geometry. This has now been deleted.


### Modelling sections - use of schema

The section(s) are considered to be part of a
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model">model</a>.
A model is established by defining
an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object, which will
contain general metadata for the model.

!!! Note
    For 2D sections, a separate <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model">model</a>
    will generally be required for each section for the reasons stated
    [above](#coordinate-systems),
    whereas 3D sections may be incorporated within an existing
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model">model</a>.

A <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model-boundary">model boundary</a>
may also be defined using an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> object.
For a section, the boundary could be the base elevation only.
If used, <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> is embedded within the parent
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#model-element">Model elements</a>
are then defined using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> objects.
Typically, an area representing a geological unit on a particular section will be one
model element.
All of the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> objects
are embedded, as an array, within
the <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.

Each <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> object has an attribute
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a>
in which the geometry object defining the element geometry is embedded.
For the areas representing geological units on a section the geometry object will normally be either an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryAreaFromLines">agsiGeometryAreaFromLines</a>
object or a closed polygon defined in a
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-supporting-files">supporting file</a>
referenced by
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>.

If <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryAreaFromLines">agsiGeometryAreaFromLines</a>
is used and AGSi recommendations are followed then both top and bottom boundary lines will be defined for each unit using a single
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryAreaFromLines">agsiGeometryAreaFromLines</a> object.
This object will embed the individual boundary lines which will normally be
defined using <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
objects referencing
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#agsi-supporting-files">supporting files</a>.

!!! Note
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryAreaFromLines">agsiGeometryAreaFromLines</a>
    objects are the 2D equivalents of the
    <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a>
    objects that can be used to define volumes from surfaces.

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.