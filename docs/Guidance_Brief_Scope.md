# AGSi Guidance / Introduction to AGSi

## What is AGSi?

This is part 1 of 3 of our *Introduction to AGSi* guide. This part describes:

- what AGSi is
- what it can/should be used for
- some fundamental concepts

[Part 2](./Guidance_Brief_General.md) will go on to talk about object models and JSON encoding in general (you may skip this if you are familiar with these).
[Part 3](./Guidance_Brief_Schema.md) will provide a tour of the AGSi schema.

*Reading time for this page is about 4 mins (reading time for all parts is 20-25 mins)*

### What is AGSi and what is it used for?

AGSi is the short name of a schema and transfer format for the exchange of ground model and interpreted data in the ground domain, which may include geotechnical engineering, geology, hydrogeology and geoenvironmental.

!!! Note
    AGSi is intended to serve all of the above, but the initial development work has focused on geotechnical engineering applications which are expected to provide the main use case.

The data collected from ground investigation and monitoring is considered to be factual data. This data is then collated, studied and processed to create interpreted data.
This may then be used to create one or more ground models, digital visual representations of the ground identifying the different strata/units. These may incorporate embedded and/or linked data.

![Typical 3D model using surfaces](./images/Guidance_3Dmodel_Neil.png)

AGSi is intended as a vehicle for the transfer of such ground models. The main use cases envisaged are:

- sharing of models between different organisations, or between different disciplines working on a project
- import of ground models into multidisciplinary BIM or GIS models
- import of ground models into analysis software

!!! Note
    The full potential of AGSi may only be leveraged as software implementations are developed. However, the nature of AGSi (JSON file with geometry mostly in linked files) is such that is possible to create processes for creating and reading AGSi files using familiar technology. For example, python scripts (which can read/write JSON) could be developed to be used alongside modelling software (used to create/view the model geometry) and spreadsheets (used to assemble model data and metadata).

Ground models are typically 3D, but 2D cross sections and simple 1D profiles are also considered to be ground models and are therefore supported by AGSi.

!!! Note
    AGSi can be used to transfer key information from geotechnical interpretative reports such as data summaries and design parameters. At present AGSi is not capable of transferring the full contents of an interpretative report. However, this is under consideration for a future version of AGSi.

AGSi should not be used for the transfer of factual data from ground investigations or monitoring. The <a href="https://www.ags.org.uk/data-format/" target="_blank">existing AGS (factual) data format</a> should be used for this. However, it is sometimes useful to include selected field observations or test results from ground investigations in models. These will sometimes comprise factual data although in other cases the data may, strictly speaking, be interpreted having have undergone some processing. AGSi includes a mechanism for incorporating all such information.


### AGSi fundamental concepts

AGSi is a high level schema that provides:

- metadata for a model, e.g. description, methodology and limitations
- the glue that binds together otherwise disparate elements of geometry into a coherent ground model
- data (properties and parameters) associated with the geometric elements

Perhaps the main thing you need to understand about AGSi is that it does NOT include schema for complex geometry, e.g. volumes, surfaces or lines. Instead, users are invited to use existing external file/transfer formats for geometry. AGSi provides a means of organising and linking to that data.

Having said that, AGSi includes schema for a few types of simple geometry that are not well served by the external formats (e.g. 1D layers and flat planes).

!!! Note
    Whilst AGSi allows freedom in the selection of file formats for geometry, it is strongly recommended that users think very carefully about what formats they use, taking software inter-operability into account. Ideally, there should be a specification in place to clarify what should be used on a particular project.

    At present, only very limited guidance on geometry file formats is provided in the AGSi documentation. However, the authors are looking to expand this guidance in the future.

### The AGSi package and AGSi files

Before diving into the schema, it is first necessary to understand the difference between an *AGSi package* and an *AGSi file*.

The **schema** defines how ground model data must be structured to comply with AGSi. This structured data is then processed into a prescribed format for transmittal, a process known as **encoding**, to produce what we call an **AGSi file**. This will be a JSON file as there is only one method of encoding defined at present.

!!! Note
    The door has been left open for future alternative encodings, but none are planned right now.

However, as you have already seen, most geometry is not included in the AGSi schema and is instead to be carried in external files. These must be identified in the *AGSi file* and a link provided to the location of each file. The preference is for these files to be provided alongside the *AGSi file*. This set of files, including the *AGSi file*, is known as an **AGSi package**.

!!! Note
    In practice, almost all real world  models will require external geometry files. Possible exceptions, i.e. all data in the AGSi file itself, are:

    - stratigraphical column (1D) models, commonly used for design
    - a model comprising only borehole data, possibly with the intent of combining this with another 3D model

Referenced document and data files may also be included within the AGSi package.

Alternatively, AGSi allows the external files to be hosted externally, with links provided. However, it is recommended that geometry and data files, on which the AGSi file may rely, are included in the AGSi package. Documents are generally not critical and in some cases external hosting of these, eg. on a project document management system, may be preferable.

The AGSi package includes one other file, called an **index file**, which will have the name *index.json*. This is a small and simple file containing some metadata that should be helpful for applications trying to use AGSi. Further details can be found <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Transfer#182-index-file">here</a>.

The above concepts, and the recommended folder structure, are illustrated in the following:

![AGSi package and file concept](./images/General_Transfer_Folders_Extra.png)

### Next

[Part 2](./Guidance_Brief_General.md) introduces object models and JSON encoding in general. If you are already familiar with these concepts, they you may skip these and go straight to [part 3](./Guidance_Brief_Schema.md) which is a tour of the AGSi schema.

### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Links to standard updated (for change of address)

First formal issue, for v1.0.0 of standard, 30/11/2022.
