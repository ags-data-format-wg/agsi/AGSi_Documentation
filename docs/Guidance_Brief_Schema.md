# AGSi Guidance / Introduction to AGSi

##  The AGSi schema

This is the final part of our *Introduction to AGSi* guide.

This part provides a tour of the AGSi schema together with information on how an AGSi file (as JSON) should be put together. We assume basic knowledge of object models and JSON. If you are not familiar with these concepts then we suggest that you first read
[part 2](./Guidance_Brief_General.md) to get up to speed.

[Part 1](./Guidance_Brief_Scope.md)  described what AGSi is, what it can/should be used for and some general concepts. [Part 2](./Guidance_Brief_General.md) talked about object models and JSON encoding in general, providing a primer for this final part.

*Reading time for this page is about 12 mins (reading time for all parts is 20-25 mins)*

!!! Note
    Links to the full AGSi documentation are included on this page. If you follow a link please use your browser back button to return to where you left off. Links to external sites should open in a new browser window.

### An overview of the schema

The AGSi schema (as of v1.0.1) includes 28 objects with a combined total of about 280 attributes.

The schema is hierarchical, ie. it includes objects embedded (nested) within other objects. In addition, some objects are linked to other objects by cross-reference.

The top level (ultimate parent object) of the schema is known as the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Root_Intro">root object</a>.
Within this <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Root_Intro">root object</a>
are found two objects that contain general metadata:
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Root_agsSchema">agsSchema</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Root_agsFile">agsFile</a>.

The remaining schema objects are organised into the following groups according to function:

* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_Intro">Project</a>
* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_Intro">Model</a>
* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_Intro">Observation</a>
* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_Intro">Geometry</a>
* <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_Intro">Data</a>

The root object contains the top level object for the Project group.
It also contains the top level object of the
Model group although in this case multiple models are permitted, within an array.

The remaining groups (Observation, Geometry and Data) comprise 'resource' objects that may be embedded within other objects in the schema.

This general concept is illustrated in the following diagram.

<br>

![Schema overall summary UML diagram](./images/ALL_overview_UML.svg)

Based on the above, an AGSi file encoded as JSON will generally have a high level structure that looks something like this:

``` json
    {
      	"agsSchema": { … },
      	"agsFile": { … },
      	"agsProject": { … Project group objects … },
      	"agsiModel": [ … Array of { … Model group objects … } ]
    }
```
In the following, we will flesh out this skeleton AGSi data file as we work through the different groups of objects.

### Model group

The most important part of the AGSi schema is the Model group. Models in AGSi are made up of **model elements** representing features of interest, such as geological units. These model elements have geometry and they may also have data (properties and/or parameters) assigned to them.

In the AGSi schema, a model is made using the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object which may contain several <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> objects.

The other objects in the Model group are:

- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelBoundary">agsiModelBoundary</a> which may be used to defined a model boundary, if required
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelAlignment">agsiModelAlignment</a> which can be used to define the line of a section, if required

The schema for the Model group of objects is illustrated below:

![Model UML diagram](./images/agsiModel_summary_UML.svg)

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> has one other object that may be embedded within it which is
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>.
As its name suggests, this object is part of the Observation group, which is described in the next section.

The geometry of a model element is defined using an object from the Geometry group (described below) which is embedded under the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement#agsigeometry">*agsiGeometry*</a>
attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a>.

Similarly, <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a>
also includes attributes for embedding data (properties and parameters).

Fleshing out the skeleton AGSi file that we started above with detail for the Model group, we end up with a structure that should look something like this:

``` json
{
    "agsSchema": { … },
    "agsFile": { … },
    "agsProject": { … Project group objects … },
    "agsiModel": [
        {    … model attributes (model metadata) … ,
            "agsiModelElement": [
                {    … element attributes (element metadata) … ,
                    "agsiGeometry": { … embedded geometry object … },
                    "agsiDataParameterValue": [{ … embedded data object(s) … }],
                },
                { … another model element … etc … }
            ],
            "agsiModelBoundary": { … boundary attributes/geometry … }
        },
        { … may be another model object here … etc … }
    ]
}
```

### Observation group

The Observation group comprises objects that can be used to describe observations, or sets thereof. These observations are included as part of an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.

In an AGSi context, **observations** comprise spatially referenced data or information provided as part of the model, or in support of it. Observations include, but are not necessarily limited to, data and information used to derive the model.

In AGSi, observations may include factual data from ground investigations, field observations, data from surveys and findings from desk study and other studies.
Such observations are most likely to comprise
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#factual-data">factual data</a>,
but there may be situations where it is appropriate to include <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_General_Definitions#interpreted-data">interpreted data</a> as observations.

The primary focus of AGSi is ground models and interpreted data. AGSi is not intended to replace or replicate the <a href="https://www.ags.org.uk/data-format/" target="_blank">existing AGS format</a>
for transfer of factual data from ground investigations (GI).
Observations in AGSi should only be used to convey significant information that adds value to the model, taking into account its intended usage.

The schema for the Observation group, which comprises just five objects, is shown and described below:

![Observation UML diagram](./images/agsiObservation_summary_UML.svg)

All observations are gathered together within one or more
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a> objects, which as noted above are embedded within the parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> object.
A typical use of <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>
would be for a ground investigation (GI).

For a GI, individual exploratory holes (of any type) can be represented using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationExpHole">agsiObservationExpHole</a> objects,
with each segment/unit of geology represented by
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationColumn">agsiObservationColumn</a> objects.
Observation group objects have only the bare minimum of attributes defined,
but include attributes that allow additional data to be assigned via embedment of Data group objects.

For observations other than those from exploratory holes, there are two generic objects than can be used: <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationPoint">agsiObservationPoint</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationShape">agsiObservationShape</a>.
The only difference between these is that the former is an observation for a defined point whereas the latter can be assigned geometry using an embedded geometry object.
Both can have simple values assigned using the *observationText*
attribute. Alternatively, structured data can be provided using embedded data objects.

Below we show how observations are incorporated in an AGSi file (simple case - embedded data objects not included).

``` json
{
    "agsSchema": { … },
    "agsFile": { … },
    "agsProject": { … Project group objects … },
    "agsiModel": [
        {    … model attributes (model metadata) … ,
            "agsiModelElement": [ { … as described above … }],
            "agsiModelBoundary": { … boundary attributes/geometry … },
            "agsiObservationSet": [
                {    … observation set metadata) … ,
                    "agsiObservationExpHole": [
                        {    … hole attributes (name, type etc.) … ,
                            "agsiObservationColumn": [
                                    { … data for first segment/unit … },
                                    { … data for next segment/unit … },
                                    { … etc … }
                            ]
                        },
                        { … data for next hole … etc …}        
                    ]
                },
                { … may be another observation set here … etc … }            
            ]
        },
        { … may be another model object here … etc … }
    ]
}
```

### Geometry group

Geometry objects are used to describe geometry.
They are resource objects that can be embedded in selected other objects, most commonly <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a>.

There are five objects in the geometry group which are shown and described below.
Any of these may be embedded within a parent object from elsewhere in the schema where permitted by the documentation.

![Geometry UML diagram](./images/agsiGeometry_summary_UML.svg)

The most important geometry object is
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a>
which is used to provide a URI (uniform resource identifier) link the external file that  contains the relevant geometry data. This object also allows for file metadata to be included (optional, but recommended).

If surfaces are used in a 3D model to define the extents of model elements, such as geological units, then use of
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryVolFromSurfaces">agsiGeometryVolFromSurfaces</a>
is recommended as this allows both top and bottom surfaces to be defined, generally using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryFromFile">agsiGeometryFromFile</a> objects which are embedded in the former. Use of this object invokes a
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_Rules#623-volumes-from-surfaces-agsigeometryvolfromsurfaces">set of rules</a> that define how the volume implied from the surfaces is formed.

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryAreaFromLines">agsiGeometryAreaFromLines</a> is a 2D equivalent of the above used to create areas from lines on cross sections.

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryLayer">agsiGeometryLayer</a> is a very simple object that directly defines geometry for a one dimensional layer based on top and/or bottom elevations. <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Geometry_agsiGeometryPlane">agsiGeometryPlane</a> is even more simple, defining a horizontal plane at a specified elevation.

Returning to our example AGSi file, you can see below how the geometry for a model element, in this case a volume from LandXML surfaces, fits in.

``` json
{
    "agsSchema": { … },
    "agsFile": { … },
    "agsProject": { … Project group objects … },
    "agsiModel": [
        {    … model attributes (model metadata) … ,
            "agsiModelElement": [
                {    … element attributes (element metadata) … ,
                    "agsiGeometry": {
                        "agsiGeometryTop": {
                            "fileURI" : "geometry/topsurfacefilename.xml",
                             … other attributes with file metadata …
                        },
                        "agsiGeometryBottom": {
                            "fileURI" : "geometry/bottomsurfacefilename.xml",
                             … other attributes with file metadata …
                        }
                    },
                    "agsiDataParameterValue": [{ … embedded data object(s) … }],
                },
                { … another model element … etc … }
                ],
            "agsiModelBoundary": { … boundary attributes/geometry … },
            "agsiObservationSet": [ { … as described above … } ] },
            ]
        },
        { … may be another model object here … etc … }
    ]
}
```

### Data group

Data group objects are used to describe properties and parameters that can be assigned to several different objects in the AGSi schema, in particular <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModelElement">agsiModelElement</a> and Observation group objects.

In AGSi we have decided to define properties and parameters in a specific way:

- A **property** is a value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values. Summaries of properties may also be reported, eg. range, mean and number of results.

- A **parameter** is an interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

The data group schema is very simple, comprising four standalone objects which may be embedded in other objects within the schema, where permitted. This means that the Data group schema looks like this - told you it was simple!


![Data UML diagram](./images/agsiData_summary_UML.svg)

Usage of the objects is as follows:

- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a> is used for parameters, which may be a single value or a profile (design line)
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a> is used for single property values, or a profile of values against, for example, depth or elevation
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertySummary">agsiDataPropertySummary</a> may be used to provide a summary of property values, e.g. minimum, maximum, mean, standard deviation, count
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyFromFile">agsiDataPropertyFromFile</a> may be used to link to a supporting file containing relevant data; typically used for more complex structured data

The parameter and property value/summary objects are all general purpose objects. By this we mean that the same object can be used to convey any type or property or parameter. We define what the property or parameter is by means of a 'code'. Rather than trying to explain it in words, here is an example of an
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a> object which is hopefully self explanatory.

``` json
{ "codeID": "UndrainedShearStrength", "valueNumeric": 100 }
```

In this case the code is also easy to understand. It also happens to be taken from the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Codes_Codelist">AGSi recommended standard list of codes</a>.
However, this will not always be the case. Therefore AGSi requires that all codes must be defined, either by reference to a standard list, or explicitly within the Project group using
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a> objects, which are collected together into parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a> objects.

For Data group objects, the code definition must also include the units of measurement, if applicable.

!!! Note
    The code definitions are located in the Project group because there are several objects within the AGSi schema that use project-wide codes.

For <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a>,
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertyValue">agsiDataPropertyValue</a> and
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataPropertySummary">agsiDataPropertySummary</a>
 a second 'code' attribute called <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue#caseid">*caseID*</a> is also available for use if required. This would typically be used where the same parameter or property is defined more than once, but with different values assigned depending on the intended use case (a common scenario in design).

This is how an <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a>
object would fit within our example AGSi file:


``` json
{
    "agsSchema": { … },
    "agsFile": { … },
    "agsProject": { … Project group objects … },
    "agsiModel": [
        {    … model attributes (model metadata) … ,
            "agsiModelElement": [
                {    … element attributes (element metadata) … ,
                    "agsiGeometry": { … as described above … },
                    "agsiDataParameterValue": [
                        { "codeID": "UndrainedShearStrength",
                          "caseID": "EC7PileDesign",
                          "valueNumeric": 100 },
                        { … may be another parameter object here … etc … }
                    ],
                },
                { … another model element … etc … }
                ],
            "agsiModelBoundary": { … boundary attributes/geometry … },
            "agsiObservationSet": [ { … as described above … } ] },
            ]
        },
        { … may be another model object here … etc … }
    ]
}
```

### Project group

Finally, we reach the Project group, although in practice this may be amongst the first data that you see in an AGSi file if the recommended order of objects is followed.

The Project group provides a home for project-wide data. This includes metadata for the project itself, but also things like codes, documents, coordinate systems and ground investigations that may be referenced from other parts of the schema.

Please be aware that, for the purposes of AGSi, the **Project** defined using the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProject">agsProject</a> object is the specific project/commission under which an AGSi file is delivered. The
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProject">agsProject</a> object includes attributes that allow the parent and ultimate parent projects to be identified.

Here is the summary schema diagram for the Project group with the objects described below.

<br>

![Project UML diagram](./images/agsProject_summary_UML.svg)

Project group objects:

- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProject">agsProject</a> contains metadata for the Project (commission); also acts as the parent for the remaining objects in this group
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectInvestigation">agsProjectInvestigation</a> may be used to provide metadata for ground investigations, which may be referenced from <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Observation_agsiObservationSet">agsiObservationSet</a>
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCoordinateSystem">agsProjectCoordinateSystem</a> is used to describe the coordinate system(s), referenced by <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Model_agsiModel">agsiModel</a> objects
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCode">agsProjectCode</a> and its parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectCodeSet">agsProjectCodeSet</a> are used to define codes used by some objects, including Data group objects as described above
- <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectDocument">agsProjectDocument</a> and its parent <a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_agsProjectDocumentSet">agsProjectDocumentSet</a> are used to describe and link to documents that may be cross-referenced from elsewhere in the schema

We are not going to go through the Project group details or provide an example here as you are hopefully getting the hang of it now (and this article is already quite long). Check out the
<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/Standard_Project_Intro">full Project group documentation</a> to find out more.


### Further reading

Obviously we could not cover everything here in this short introductory guide but hopefully you now know enough to get going with AGSi, and will be better placed to make sense of the full documentation.

Where you go next is up to you. You may wish to continue reading the remaining *Guidance*, which goes into more detail on selected subjects, with some examples provided. Alternatively you may prefer to dive straight into the *Standard*. The choice is yours. It depends on your needs and desires.


### Full schema diagram

To finish of this guide, here is a link to a diagram showing the entire AGSi schema (current version - this is a link to the Standard). Enjoy!

<a href="https://ags-data-format-wg.gitlab.io/agsi/agsi_standard/latest/images/ALL_full_UML.svg" target="_blank">Click here to see the full AGSi schema (opens in a new browser window)</a>.


### Version history

Minor update, for v1.0.1 of standard, June 2024:

- Reference to latest version updated (to 1.0.1) 
- Update to paragraph on *caseID* in Data section
- Links to standard updated (for change of address)
- Link from 'set of rules' corrected
- Minor typo correction

First formal issue, for v1.0.0 of standard, 30/11/2022.